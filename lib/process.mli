module type Monad = sig
  type 'a t

  val return : 'a -> 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module Make : functor
  (M : Monad)
  (Message : sig
     type t
   end)
  -> sig
  module Internal : sig
    type t

    val elapse : t -> float -> t

    val empty : t
  end

  type 'a t

  val sleep : float -> unit t

  val now : float t

  val return : 'a -> 'a t

  val ( let* ) : 'a t -> ('a -> 'b t) -> 'b t

  val get_id : Thread_id.t t

  val send : Message.t -> Thread_id.t -> float -> unit t

  val recv : (Message.t * Thread_id.t) t

  val try_recv : (Message.t * Thread_id.t) option t

  val perform : 'a M.t -> 'a t

  val join : unit t -> unit t -> unit t

  val async : unit t -> unit t

  type sleep_handler :=
    Internal.t ->
    min_sleep:float ->
    cont:(Internal.t -> Internal.t M.t) ->
    Internal.t M.t

  val default_sleep_handler : sleep_handler

  val run :
    'a. ('a t * Thread_id.t) array -> sleep_handler -> 'a Thread_id.Map.t M.t

  val list_iter : ('a -> unit t) -> 'a list -> unit t

  val list_fold_left : ('acc -> 'b -> 'acc t) -> 'acc -> 'b list -> 'acc t
end

module Identity : Monad with type 'a t = 'a

module Sampling : Monad with type 'a t = Random.State.t -> 'a
