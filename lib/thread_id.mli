type t = private int

val equal : t -> t -> bool

val compare : t -> t -> int

val pp : Format.formatter -> t -> unit

val of_int : int -> t

val gen : unit -> t

module Map : sig
  include Map.S with type key = t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module Set : sig
  include Set.S with type elt = t

  val pp : Format.formatter -> t -> unit
end
