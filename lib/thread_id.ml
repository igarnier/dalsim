type t = int

let equal = Int.equal

let compare = Int.compare

let pp = Format.pp_print_int

let of_int = Fun.id

let gen =
  let x = ref 0 in
  fun () ->
    let v = !x in
    incr x ;
    v

module Map = struct
  include Map.Make (Int)

  let pp_pair pp_x pp_y fmtr (x, y) =
    Format.fprintf fmtr "(%a, %a)" pp_x x pp_y y

  let pp pp_elt fmtr map =
    let elts = bindings map in
    Format.pp_print_list
      ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",@ ")
      (pp_pair Format.pp_print_int pp_elt)
      fmtr
      elts
end

module Set = struct
  include Set.Make (Int)

  let pp fmtr set =
    let elts = elements set in
    Format.fprintf
      fmtr
      "@[[ %a ]@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",@ ")
         Format.pp_print_int)
      elts
end
