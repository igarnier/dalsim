module type Monad = sig
  type 'a t

  val return : 'a -> 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t
end

module Make
    (M : Monad) (Message : sig
      type t
    end) =
struct
  type ('state, 'a) cont =
    Thread_id.t ->
    'state ->
    ('state -> 'state M.t) ->
    ('state -> 'a -> 'state M.t) ->
    'state M.t

  type _ ex = Ex : int * ('state -> unit -> 'state M.t) -> 'state ex

  type _ ex_msg =
    | Ex_msg :
        int * ('state -> Message.t * Thread_id.t -> 'state M.t)
        -> 'state ex_msg

  let uid (Ex (id, _)) = id

  let gen =
    let x = ref 0 in
    fun () ->
      let v = !x in
      incr x ;
      v

  let ex k = Ex (gen (), k)

  let ex_msg k = Ex_msg (gen (), k)

  module rec Monad : sig
    type 'a t = (Internal.t, 'a) cont

    type nonrec ex = Internal.t ex

    type nonrec ex_msg = Internal.t ex_msg
  end = struct
    type 'a t = (Internal.t, 'a) cont

    type nonrec ex = Internal.t ex

    type nonrec ex_msg = Internal.t ex_msg
  end

  and Internal : sig
    type t

    type status =
      | Empty
      | All_asleep of { duration : float }
      | Ready of (Monad.ex * t)

    val empty : t

    val push_sleeper : t -> Monad.ex -> float -> t

    val push_waiter_on_recv : t -> Thread_id.t -> Monad.ex_msg -> t

    val get_next : t -> status

    val now : t -> float

    val elapse : t -> float -> t

    val send :
      t ->
      delay:float ->
      msg:Message.t ->
      src:Thread_id.t ->
      dst:Thread_id.t ->
      t

    val recv : t -> dst:Thread_id.t -> (Message.t * Thread_id.t * t) option

    type join_id

    val declare_join : t -> int -> join_id * t

    val thread_terminated : t -> join_id -> bool * t

    val set_result : t -> Thread_id.t -> 'a -> t

    val unsafe_get_results : t -> 'a Thread_id.Map.t
  end = struct
    module Sleeper = struct
      type t = Monad.ex

      let compare a b = Int.compare (uid a) (uid b)
    end

    module Internal_message = struct
      type t = { uid : int; sender : Thread_id.t; msg : Message.t }

      let compare { uid = id1; _ } { uid = id2; _ } = Int.compare id1 id2
    end

    module KQueue = Psq.Make (Sleeper) (Float)
    module MQueue = Psq.Make (Internal_message) (Float)

    type join_id = Join of int [@@unboxed]

    module Join_map = Map.Make (struct
      type t = join_id

      let compare (Join i) (Join j) = Int.compare i j
    end)

    type res = Res : 'a -> res

    type t =
      { now : float;
        kqueue : KQueue.t;
        mqueues : (Monad.ex_msg list * MQueue.t) Thread_id.Map.t;
        joins : int Join_map.t;
        res : res Thread_id.Map.t
      }

    type status =
      | Empty
      | All_asleep of { duration : float }
      | Ready of (Monad.ex * t)

    let empty =
      { now = 0.0;
        kqueue = KQueue.empty;
        mqueues = Thread_id.Map.empty;
        joins = Join_map.empty;
        res = Thread_id.Map.empty
      }

    let push_sleeper state sleeper duration =
      assert (duration >= 0.0) ;
      let wakeup_at = state.now +. duration in
      let kqueue = KQueue.add sleeper wakeup_at state.kqueue in
      { state with kqueue }

    let push_waiter_on_recv state recvr k =
      let mqueues = state.mqueues in
      match Thread_id.Map.find_opt recvr mqueues with
      | None ->
          let mqueues = Thread_id.Map.add recvr ([k], MQueue.empty) mqueues in
          { state with mqueues }
      | Some (waiters, q) -> (
          (* Invariant: here,
             - either the mqueue is empty (previous case)
             - the queue is nonempty but the messages are still in flight,
               in which case waiters = [] *)
          match MQueue.pop q with
          | None ->
              let mqueues = Thread_id.Map.add recvr (k :: waiters, q) mqueues in
              { state with mqueues }
          | Some (({ msg; sender; _ }, arrival_time), rest) ->
              assert (waiters = []) ;
              (* assert (arrival_time >= state.now) ; *)
              let (Ex_msg (_, k)) = k in
              (* Move consumer to sleep queue *)
              let wait = Float.max 0.0 (arrival_time -. state.now) in
              let state =
                push_sleeper
                  state
                  (ex (fun state () -> k state (msg, sender)))
                  wait
              in
              let mqueues = Thread_id.Map.add recvr (waiters, rest) mqueues in
              { state with mqueues })

    let get_next state =
      match KQueue.pop state.kqueue with
      | None -> Empty
      | Some ((sleeper, wakeup_at), kqueue) ->
          if wakeup_at <= state.now then
            let rest = { state with kqueue } in
            Ready (sleeper, rest)
          else All_asleep { duration = wakeup_at -. state.now }

    let now state = state.now

    let elapse state dt = { state with now = state.now +. dt }

    let send state ~delay ~msg ~src ~dst =
      assert (delay >= 0.0) ;
      let now = state.now in
      let arrive_at = now +. delay in
      let mqueues = state.mqueues in
      match Thread_id.Map.find_opt dst mqueues with
      | None ->
          let q = MQueue.sg { uid = gen (); sender = src; msg } arrive_at in
          let mqueues = Thread_id.Map.add dst ([], q) mqueues in
          { state with mqueues }
      | Some ([], q) ->
          let q = MQueue.add { uid = gen (); sender = src; msg } arrive_at q in
          let mqueues = Thread_id.Map.add dst ([], q) mqueues in
          { state with mqueues }
      | Some (Ex_msg (_, k) :: tl, q) ->
          (* Invariant: if there are sleepers, the queue is empty *)
          assert (MQueue.is_empty q) ;
          (* Move sleeper to sleep queue *)
          let state =
            push_sleeper state (ex (fun state () -> k state (msg, src))) delay
          in
          let mqueues = Thread_id.Map.add dst (tl, q) mqueues in
          { state with mqueues }

    let recv state ~dst : (Message.t * Thread_id.t * t) option =
      let mqueues = state.mqueues in
      match Thread_id.Map.find_opt dst mqueues with
      | None -> None
      | Some (sleepers, q) -> (
          match MQueue.pop q with
          | None -> None
          | Some (({ msg; sender; _ }, arrival_time), rest) ->
              (* Invariant: if the message queue is nonempty, there's no msg waiter *)
              assert (sleepers = []) ;
              if arrival_time >= state.now then
                Some
                  ( msg,
                    sender,
                    { state with
                      mqueues = Thread_id.Map.add dst ([], rest) state.mqueues
                    } )
              else None)

    let declare_join : t -> int -> join_id * t =
     fun state count ->
      let join_id = Join (gen ()) in
      (join_id, { state with joins = Join_map.add join_id count state.joins })

    let thread_terminated : t -> join_id -> bool * t =
     fun state id ->
      match Join_map.find_opt id state.joins with
      | None | Some 0 -> assert false
      | Some 1 ->
          let state = { state with joins = Join_map.remove id state.joins } in
          (true, state)
      | Some c ->
          let state =
            { state with joins = Join_map.add id (c - 1) state.joins }
          in
          (false, state)

    let set_result state thread_id res =
      { state with res = Thread_id.Map.add thread_id (Res res) state.res }

    let unsafe_get_results : type a. t -> a Thread_id.Map.t =
     fun state -> Thread_id.Map.map (fun (Res x) -> Obj.magic x) state.res
  end

  type 'a t = 'a Monad.t

  let sleep sleep_duration : unit t =
   fun _id state es k ->
    let state = Internal.push_sleeper state (ex k) sleep_duration in
    (es [@tailcall]) state

  let now : float t =
   fun _id state _es k -> (k [@tailcall]) state (Internal.now state)

  let return : 'a. 'a -> 'a t = fun x _id state _es k -> (k [@tailcall]) state x

  let ( let* ) : 'a 'b. 'a t -> ('a -> 'b t) -> 'b t =
   fun m f id state es k ->
    (m [@tailcall]) id state es (fun state x -> (f [@tailcall]) x id state es k)
   [@@inline]

  let get_id : Thread_id.t t = fun id state _es k -> (k [@tailcall]) state id

  let send msg dst delay : unit t =
   fun id state _es k ->
    let state = Internal.send state ~delay ~msg ~src:id ~dst in
    (k [@tailcall]) state ()

  let recv : (Message.t * Thread_id.t) t =
   fun id state es k ->
    match Internal.recv state ~dst:id with
    | None ->
        (* [recv] returns None if msg queue is empty
           Add this thread to the list of msg waiters on this queue *)
        let state = Internal.push_waiter_on_recv state id (ex_msg k) in
        (es [@tailcall]) state
    | Some (msg, sender, state) -> (k [@tailcall]) state (msg, sender)

  let try_recv : (Message.t * Thread_id.t) option t =
   fun id state _es k ->
    match Internal.recv state ~dst:id with
    | None -> (k [@tailcall]) state None
    | Some (msg, sender, state) -> (k [@tailcall]) state (Some (msg, sender))

  let perform : 'a M.t -> 'a t =
   fun eff _id state _es k ->
    (M.bind [@tailcall]) eff (fun b -> (k [@tailcall]) state b)

  let join : unit t -> unit t -> unit t =
   fun m1 m2 id state es k ->
    let (join_id, state) = Internal.declare_join state 2 in
    let finalizer state () =
      let (all_finished, state) = Internal.thread_terminated state join_id in
      if all_finished then (k [@tailcall]) state () else es state
    in
    let m1 state () = m1 id state es finalizer in
    let m2 state () = m2 id state es finalizer in
    (* Note: this breaks the property that the external scheduler only sees
       sleeping promises *)
    let state = Internal.push_sleeper state (ex m1) 0.0 in
    let state = Internal.push_sleeper state (ex m2) 0.0 in
    (es [@tailcall]) state

  let async : unit t -> unit t =
   fun m id state es k ->
    let m state () = m id state es (fun state () -> es state) in
    let state = Internal.push_sleeper state (ex m) 0.0 in
    (k [@tailcall]) state ()

  let default_sleep_handler state ~min_sleep ~cont =
    cont (Internal.elapse state min_sleep)

  let run : type a. (a t * Thread_id.t) array -> _ -> a Thread_id.Map.t M.t =
   fun xs es ->
    let rec loop state =
      (* TODO: fully trampolined style *)
      match Internal.get_next state with
      | Internal.Empty -> M.return state
      | Internal.All_asleep { duration } ->
          es state ~min_sleep:duration ~cont:loop
      | Internal.Ready (Ex (_, k), state) -> k state ()
    in
    let state =
      Array.fold_left
        (fun state (x, thread_id) ->
          Internal.push_sleeper
            state
            (ex (fun state () ->
                 x thread_id state loop @@ fun state x ->
                 let state = Internal.set_result state thread_id x in
                 loop state))
            0.0)
        Internal.empty
        xs
    in
    M.bind (loop state) @@ fun state ->
    M.return (Internal.unsafe_get_results state)

  (* let run : (unit t * Thread_id.t) array -> _ -> Internal.t -> unit M.t =
   *  fun xs es state ->
   *   let rec loop state =
   *     (\* TODO: fully trampolined style *\)
   *     match Internal.get_next state with
   *     | Internal.Empty -> exit 0
   *     | Internal.All_asleep { duration } ->
   *         M.bind (es state ~min_sleep:duration) loop
   *     | Internal.Ready (Ex (_, k), state) -> M.bind (k state ()) loop
   *     (\* k state () (fun state_m -> M.bind state_m loop )*\)
   *   in
   *   let state =
   *     Array.fold_left
   *       (fun state (x, thread_id) ->
   *         Internal.push_sleeper
   *           state
   *           (ex (fun state () ->
   *                x thread_id state loop @@ fun state () -> loop state))
   *           0.0)
   *       state
   *       xs
   *   in
   *   loop state *)

  let list_iter : ('a -> unit t) -> 'a list -> unit t =
   fun f list ->
    let rec loop l =
      match l with
      | [] -> return ()
      | hd :: tl ->
          let* () = f hd in
          loop tl
    in
    loop list

  let list_fold_left : 'acc. ('acc -> 'b -> 'acc t) -> 'acc -> 'b list -> 'acc t
      =
   fun f acc list ->
    let rec loop l acc =
      match l with
      | [] -> return acc
      | hd :: tl ->
          let* acc = f acc hd in
          loop tl acc
    in
    loop list acc
end
[@@inline]

(* Some typical monads for convenience *)

module Identity = struct
  type 'a t = 'a

  let return x = x

  let bind m f = f m
end

module Sampling = struct
  type 'a t = Random.State.t -> 'a

  let return x _rng_state = x

  let bind m f rng_state =
    let m = m rng_state in
    f m rng_state
end
