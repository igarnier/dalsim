type level = Debug | Info

type t = { log : 'a. level -> ('a, Format.formatter, unit) format -> 'a }
[@@unboxed]

let null = { log = (fun _ -> Format.ifprintf Format.std_formatter) }

let std = { log = (function Debug -> Format.eprintf | Info -> Format.printf) }

let file ?debug fname =
  let info_chan = open_out fname in
  let info_fmtr = Format.formatter_of_out_channel info_chan in
  let debug_chan_opt = Option.map open_out debug in
  let debug_fmtr =
    match debug_chan_opt with
    | None -> info_fmtr
    | Some oc -> Format.formatter_of_out_channel oc
  in
  let logger =
    { log =
        (function
        | Debug -> Format.fprintf debug_fmtr | Info -> Format.fprintf info_fmtr)
    }
  in
  Gc.finalise
    (fun _ ->
      close_out info_chan ;
      Option.iter close_out debug_chan_opt)
    logger ;
  logger
