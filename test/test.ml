open Des
open Process
module P = Make (Identity) (String)
open P

let rec pinger c =
  if c = 0 then return ()
  else
    let* () = send "ping" (Thread_id.of_int 1) 1.0 in
    Format.printf "sent ping@." ;
    let* (msg, sender) = recv in
    Format.printf "0 recv %s from %a@." msg Thread_id.pp sender ;
    let* () = sleep 1.0 in
    pinger (c - 1)

let rec ponger c =
  if c = 0 then return ()
  else
    let* (msg, sender) = recv in
    Format.printf "1 recv %s from %a@." msg Thread_id.pp sender ;
    let* () = send "pong" (Thread_id.of_int 0) 1.0 in
    Format.printf "sent pong@." ;
    let* () = sleep 1.0 in
    ponger (c - 1)

let _thread =
  run
    [| (pinger 5, Thread_id.of_int 0); (ponger 5, Thread_id.of_int 1) |]
    (fun state ~min_sleep ~cont -> cont (Internal.elapse state min_sleep))
