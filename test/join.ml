open Des
open Process
module P = Make (Identity) (String)

let rec loop msg c =
  let open P in
  if c = 0 then return ()
  else (
    Format.printf "%s@." msg ;
    let* () = sleep 1.0 in
    loop msg (c - 1))

let main =
  let open P in
  join (loop "A" 12) (join (loop "B" 6) (loop "C" 3))

let _thread =
  let open P in
  run
    [| (main, Thread_id.of_int 0) |]
    (fun state ~min_sleep ~cont -> cont (Internal.elapse state min_sleep))
