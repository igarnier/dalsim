open Des
open Process
module P = Make (Sampling) (String)
open P

let rec writer n =
  if n = 0 then return ()
  else
    let* delay = perform (fun rng -> Random.State.float rng 1.0) in
    let* () = send "ok" (Thread_id.of_int 1) delay in
    writer (n - 1)

let rec reader n =
  if n = 0 then return ()
  else
    let* (_m, _) = recv in
    let* () =
      async
        (let* _ = recv in
         return ())
    in
    let* (_, _) = recv in
    reader (n - 1)

let computation =
  run
    [| (writer 1000, Thread_id.of_int 0); (reader 500, Thread_id.of_int 1) |]
    (fun state ~min_sleep ~cont -> cont (Internal.elapse state min_sleep))

let _ = computation (Random.State.make_self_init ())
