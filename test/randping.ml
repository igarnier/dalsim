open Des
open Process
module P = Make (Sampling) (String)
open P

let rec pinger c =
  if c = 0 then return ()
  else
    let* b = perform Random.State.bool in
    let* () = send (Printf.sprintf "ping %b" b) (Thread_id.of_int 1) 1.0 in
    Format.printf "sent ping@." ;
    let* (msg, sender) = recv in
    Format.printf "0 recv %s from %a@." msg Thread_id.pp sender ;
    let* () = sleep 1.0 in
    pinger (c - 1)

let rec ponger c =
  if c = 0 then return ()
  else
    let* (msg, sender) = recv in
    Format.printf "1 recv %s from %a@." msg Thread_id.pp sender ;
    let* () = send "pong" (Thread_id.of_int 0) 1.0 in
    Format.printf "sent pong@." ;
    let* () = sleep 1.0 in
    ponger (c - 1)

let computation =
  run
    [| (pinger 5, Thread_id.of_int 0); (ponger 5, Thread_id.of_int 1) |]
    (fun state ~min_sleep ~cont -> cont (Internal.elapse state min_sleep))

let _ = computation (Random.State.make_self_init ())
