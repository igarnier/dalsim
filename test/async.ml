open Des
open Process
module P = Make (Identity) (String)

let rec subclock dt c =
  let open P in
  if c = 0 then return ()
  else
    let* now in
    Format.printf "subclock %f@." now ;
    let* () = sleep dt in
    subclock dt (c - 1)

let rec clock dt c =
  let open P in
  if c = 0 then return ()
  else
    let* () = async (subclock 0.1 3) in
    let* now in
    Format.printf "main %f@." now ;
    let* () = sleep dt in
    clock dt (c - 1)

let main = clock 1.0 5

let _thread =
  let open P in
  run
    [| (main, Thread_id.of_int 0) |]
    (fun state ~min_sleep ~cont -> cont (Internal.elapse state min_sleep))
