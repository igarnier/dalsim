open Des
open Messages

let connected_count state = Thread_id.Set.cardinal state.State.peers

let compare_known_point_info (state : State.t) (topics : Topic_set.t) (p1, ks1)
    (p2, ks2) =
  (* The order is such that a "bad" point is greater that a lesser point so that
     when sorting a list of point, better points come first.

     The firsts will be the peers with the more queried topics, In case of a
     draw on the number of topics covered, firsts will be longer-lasting
     connections then most recently disconnected peers.

     If topics is th empty set, it boils down to only take connection age into
     account. *)
  let ({ topics = topics1; _ } : State.known_state) = ks1 in
  let ({ topics = topics2; _ } : State.known_state) = ks2 in
  let compare_topics p1 p2 on_draw =
    let topics_score1 = Topic_set.(cardinal @@ inter topics topics1)
    and topics_score2 = Topic_set.(cardinal @@ inter topics topics2) in
    let r = Int.compare topics_score2 topics_score1 in
    if r = 0 then on_draw p1 p2 else r
  in
  let compare_last_seen p1 p2 =
    match (State.last_seen state p1, State.last_seen state p2) with
    | (None, None) -> 0
    | (Some _, None) -> 1
    | (None, Some _) -> -1
    | (Some time1, Some time2) -> (
        match Float.compare time1 time2 with 0 -> 0 | x -> x)
  in
  let compare_best_connection p1 p2 =
    let disconnected1 = State.is_peer_disconnected state p1
    and disconnected2 = State.is_peer_disconnected state p2 in
    match (disconnected1, disconnected2) with
    | (false, false) -> compare_last_seen p1 p2
    | (false, true) -> -1
    | (true, false) -> 1
    | (true, true) -> compare_last_seen p2 p1
  in
  compare_topics p1 p2 compare_best_connection

let sample best other points =
  if best < 0 || other < 0 then raise (Invalid_argument "P2p_pool.sample") ;
  let (best_points, rest) = Helpers.split_n best points in
  (* TODO: go through the monad properly *)
  let rest = Helpers.shuffle rest (Random.get_state ()) in
  let other_points = Helpers.take_n other rest in
  List.rev_append best_points other_points

let list_known_points state topics ?(size = 50) () =
  if size < 0 then invalid_arg "list_known_points"
  else
    let other = size * 2 / 5 in
    let best = size - other in
    Thread_id.Map.bindings state.known
    |> List.sort (compare_known_point_info state topics)
    |> sample best other

module Topo = struct
  type target = { to_contact : int } [@@deriving show]

  type action = Try_to_contact of target | Kill of Thread_id.t list | Noop
  [@@deriving show]

  let check (state : State.t) =
    let c = connected_count state in
    let target = state.config.target_conns in
    let delta = c - target in
    if delta = 0 then Noop
    else if delta > 0 then
      let to_kill =
        Seq.take delta (Thread_id.Set.to_seq state.peers) |> List.of_seq
      in
      Kill to_kill
    else Try_to_contact { to_contact = -delta }

  let target_reached _ { to_contact } = to_contact <= 0

  (* [connectable t ~start ~target seen_points] selects at most
     [target] connections candidates from the known points, not in [seen]
     points.

     TODO: it'd be much better if we folded over the known points in a random order
  *)
  let connectable (state : State.t) ~start ~target ~seen_points =
    let (_, connectable, seen) =
      state.logger.log Debug "%a: connectable@." Thread_id.pp state.self ;
      Thread_id.Map.fold
        (fun known _topics (c, connectable, seen) ->
          let recently_failed =
            match State.get_last_contacted state known with
            | None -> false
            | Some last ->
                (* Hackish, better to get rid of [start] altogether? *)
                abs_float (last -. start) < 5.0
          in
          if
            recently_failed
            || Thread_id.Set.mem known state.peers
            || Thread_id.Set.mem known seen
            || c >= target
          then (c, connectable, Thread_id.Set.add known seen)
          else
            let () =
              state.logger.log
                Debug
                "%a connecting to %a@."
                Thread_id.pp
                state.self
                Thread_id.pp
                known
            in
            (c + 1, known :: connectable, Thread_id.Set.add known seen))
        state.known
        (0, [], seen_points)
    in
    (connectable, seen)

  let update_target _ { to_contact } ~established =
    { to_contact = Int.max 0 (to_contact - established) }

  let ask_for_more_contacts (state : State.t) =
    let open Lang in
    let () =
      state.logger.log
        Debug
        "%a: asking for more contacts@."
        Thread_id.pp
        state.self
    in
    let* now in
    let dt = now -. state.last_bootstrap_message in
    let wait = state.config.time_between_looking_for_peers -. dt in
    let* () =
      if wait > 0.0 then
        let () =
          state.logger.log
            Debug
            "%a: sleeping %f@."
            Thread_id.pp
            state.self
            wait
        in
        sleep wait
      else return ()
    in
    let* () = State.broadcast_bootstrap_message state in
    let () =
      state.logger.log
        Debug
        "%a: broadcasted bootstrap message@."
        Thread_id.pp
        state.self
    in
    let* last_bootstrap_message = Lang.now in
    let state = { state with last_bootstrap_message } in
    return state
end

module Maintenance = struct
  let send_swap_request (state : State.t) =
    let open Lang in
    let* now in
    let dt = now -. state.last_swap_request in
    if dt < state.config.swap_linger then return state
    else
      (* TODO: go through the monad properly *)
      let peers = Thread_id.Set.to_seq state.peers |> Array.of_seq in
      if Array.length peers < 2 then return state
      else
        let () = Helpers.shuffle_array peers (Random.get_state ()) in
        let recipient = peers.(0) in
        let proposed = peers.(1) in
        let state =
          State.add_pending_swap state ~swapped:proposed ~with_:recipient
        in
        let* delay = perform (Config.delay state.config) in
        let* () = send (Messages.Swap_request proposed) recipient delay in
        let state = { state with last_swap_request = now } in
        return state

  let establish (state : State.t) contactable =
    let open Lang in
    list_fold_left
      (fun (state : State.t) point ->
        let* now in
        state.logger.log
          Debug
          "%a.%f: establishing connection with %a (%f)@."
          Thread_id.pp
          state.self
          now
          Thread_id.pp
          point
          now ;
        let* delay = perform (Config.delay state.config) in
        let* () = send Messages.Connect point delay in
        let state = State.add_pending_connection state point ~now in
        return state)
      state
      contactable
  (* List.fold_left try_to_connect (return ()) contactable *)

  let rec try_to_contact_loop (state : State.t) ~start ~seen_points target =
    let open Lang in
    state.logger.log Debug "%a: try-to-contact-loop@." Thread_id.pp state.self ;
    if Topo.target_reached state target then (
      state.logger.log
        Debug
        "%a: try-to-contact-loop: success@."
        Thread_id.pp
        state.self ;
      return (state, true))
    else
      let (candidates, seen_points) =
        Topo.connectable state ~start ~target:target.to_contact ~seen_points
      in
      if candidates = [] then (
        state.logger.log
          Debug
          "%a: try-to-contact-loop: failure@."
          Thread_id.pp
          state.self ;
        return (state, false))
      else
        let* state = establish state candidates in
        try_to_contact_loop
          state
          ~start
          ~seen_points
          (Topo.update_target
             state
             target
             ~established:(List.length candidates))

  let try_to_contact state target =
    let seen_points = Thread_id.Set.empty in
    try_to_contact_loop state target ~seen_points

  let rec do_maintain state =
    let open Lang in
    let action = Topo.check state in
    match action with
    | Try_to_contact target ->
        let () =
          state.logger.log
            Debug
            "%a: %a@."
            Thread_id.pp
            state.self
            Topo.pp_action
            action
        in
        let* start = now in
        let* (state, success) = try_to_contact state target ~start in
        if success then
          let () =
            state.logger.log
              Debug
              "%a: try-to-contact: success@."
              Thread_id.pp
              state.self
          in
          return state
        else
          let () =
            state.logger.log
              Debug
              "%a: try-to-contact: fail@."
              Thread_id.pp
              state.self
          in
          let* state = Topo.ask_for_more_contacts state in
          (* TODO: unlike the real p2p layer, we quit here *)
          return state
    | Kill connections ->
        let () = state.logger.log Debug "%a: kill@." Thread_id.pp state.self in
        let* state = list_fold_left State.disconnect state connections in
        do_maintain state
    | Noop ->
        let () =
          state.logger.log Debug "%a: try-to-contact@." Thread_id.pp state.self
        in
        (* end of maintenance when enough users have been reached *)
        return state

  let maintenance state =
    let open Lang in
    let* state = send_swap_request state in
    state.logger.log Debug "%a: swap request sent@." Thread_id.pp state.self ;
    do_maintain state
end

let log_send (state : State.t) msg dst delay =
  state.logger.log
    Debug
    "%a: sent %a to %a@."
    Thread_id.pp
    state.self
    Messages.pp
    msg
    Thread_id.pp
    dst ;
  Lang.send msg dst delay

let node ?(logger = Log.null) (config : Config.t) self =
  let open Lang in
  let rec message_handler (state : State.t) =
    let* now in
    state.logger.log
      Info
      {|{ "now": %f, "id": %a, "neighbourhood": %a }@.|}
      now
      Thread_id.pp
      self
      Thread_id.Set.pp
      state.peers ;
    state.logger.log Debug "%a.%f: waiting on message@." Thread_id.pp self now ;
    let* (msg, sender) = recv in
    let () =
      state.logger.log
        Debug
        "%a: got %a from %a@."
        Thread_id.pp
        self
        Messages.pp
        msg
        Thread_id.pp
        sender
    in
    let open Messages in
    let* state =
      match msg with
      | Connect -> handle_connect state sender
      | Nack alternatives -> handle_nack state alternatives sender
      | Ack -> handle_ack state sender
      | Disconnect -> State.disconnect state sender
      | Bootstrap topics ->
          let* () = advertise_peers_matching_topics state topics sender in
          return state
      | Advertise peers -> register_peers state peers
      | Swap_request thread_id -> handle_swap_request state thread_id sender
      | Swap_ack thread_id -> handle_swap_ack state thread_id sender
      | UpdateTopics topic_diff ->
          let state = handle_update_topic state topic_diff sender in
          return state
      | Internal_maintenance_clock ->
          let* state = Maintenance.maintenance state in
          let () =
            state.logger.log
              Debug
              "%a: exiting maintenance@."
              Thread_id.pp
              state.self
          in
          return state
    in
    message_handler state
  and handle_connect (state : State.t) sender =
    assert (not (Thread_id.equal state.self sender)) ;
    let () =
      state.logger.log
        Debug
        "%a: got Connect from %a - entry@."
        Thread_id.pp
        state.self
        Thread_id.pp
        sender
    in
    if Thread_id.Set.mem sender state.peers then
      (* ignore - should greylist? *)
      let () =
        state.logger.log
          Debug
          "%a: got Connect from %a, already in peers@."
          Thread_id.pp
          state.self
          Thread_id.pp
          sender
      in
      return state
    else
      let c = connected_count state in
      let target = config.target_conns in
      if c < target then
        (* Accept *)
        let () =
          state.logger.log
            Debug
            "%a: got Connect from %a, accept@."
            Thread_id.pp
            state.self
            Thread_id.pp
            sender
        in
        let* now in
        let state = State.add_peer state sender now in
        let* delay = perform (Config.delay state.config) in
        let* () = log_send state Messages.Ack sender delay in
        return state
      else
        (* Reject *)
        let () =
          state.logger.log
            Debug
            "%a: got Connect from %a, reject@."
            Thread_id.pp
            state.self
            Thread_id.pp
            sender
        in
        (* TODO: subsample state.known *)
        let known =
          Thread_id.Map.map
            (fun (s : State.known_state) -> s.topics)
            state.known
        in
        let* delay = perform (Config.delay state.config) in
        let* () = log_send state (Messages.Nack known) sender delay in
        return state
  and handle_nack state alternatives sender =
    if not (Thread_id.Set.mem sender state.pending_connections) then
      (* Ignore - should greylist or simulation failure? *)
      return state
    else
      let state = State.remove_pending_connection state sender in
      let state =
        Thread_id.Map.fold
          (fun tid topics state ->
            if Thread_id.equal tid state.State.self then state
            else
              let known_state =
                match State.get_known state tid with
                | exception Not_found -> { State.empty_known with topics }
                | { State.topics = t; last_seen } ->
                    { topics = Topic_set.union t topics; last_seen }
              in
              State.set_known state tid known_state)
          alternatives
          state
      in
      return state
  and handle_ack state sender =
    if not (Thread_id.Set.mem sender state.pending_connections) then
      (* Ignore - should greylist or simulation failure? *)
      return state
    else
      let state = State.remove_pending_connection state sender in
      let* now in
      let state = State.add_peer state sender now in
      (* TODO: trigger maintenance if OOB *)
      return state
  and advertise_peers_matching_topics (state : State.t) topics sender =
    let points =
      list_known_points state topics ()
      |> List.to_seq
      |> Seq.map (fun (id, (ks : State.known_state)) -> (id, ks.topics))
      |> Thread_id.Map.of_seq
    in
    let* delay = perform (Config.delay state.config) in
    send (Messages.Advertise points) sender delay
  and register_peers (state : State.t) peers =
    let peers =
      Thread_id.Map.map (fun topics -> { State.topics; last_seen = None }) peers
    in
    let known =
      Thread_id.Map.merge
        (fun thread_id x y ->
          if Thread_id.equal thread_id state.self then None
          else
            match (x, y) with
            | (Some _, _) ->
                (* Peer already known *)
                x
            | (None, Some _) ->
                (* Unknown peer *)
                (* In the real p2p, this can unblock the Topo.Discovery.ask_for_more_contacts *)
                y
            | (None, None) -> None)
        state.known
        peers
    in
    return { state with known }
  and handle_swap_request (state : State.t) new_point sender =
    let* now in
    let dt = now -. state.last_swap_request in
    if
      dt < state.config.swap_linger
      || Thread_id.equal new_point state.State.self
    then (* Ignore *)
      return state
    else
      (* TODO: converting from seq each time is inefficient *)
      let peers = Array.of_seq (Thread_id.Set.to_seq state.peers) in
      if Array.length peers <= 1 then return state
      else
        (* TODO: should not Ack before connect succeeded *)
        let* peer = perform (Stats.Gen.uniform peers) in
        let* delay = perform (Config.delay state.config) in
        let* () = log_send state Messages.Disconnect peer delay in
        let* delay = perform (Config.delay state.config) in
        let* () = log_send state (Messages.Swap_ack peer) sender delay in
        let* delay = perform (Config.delay state.config) in
        let* () = log_send state Messages.Connect new_point delay in
        let state = State.remove_peer state peer now in
        let state = State.add_pending_connection state new_point ~now in
        let state = { state with last_swap_request = now } in
        return state
  and handle_swap_ack state new_point sender =
    (* we must have performed a swap request *)
    match State.pending_swap state ~with_:sender with
    | None ->
        (* No pending swap with [thread_id], ignore
           TODO: greylist/simulation error
        *)
        return state
    | Some candidate ->
        if Thread_id.equal state.State.self new_point then return state
        else
          let* delay = perform (Config.delay state.config) in
          let* () = log_send state Messages.Disconnect candidate delay in
          let* delay = perform (Config.delay state.config) in
          let* () = log_send state Messages.Connect new_point delay in
          let* now in
          let state = State.add_pending_connection state new_point ~now in
          let state = State.remove_pending_swap state ~with_:sender in
          let state = State.remove_peer state candidate now in
          return state
  and handle_update_topic (state : State.t) topic_diff sender =
    match State.get_known_topics state sender with
    | exception Not_found ->
        (* Unknown peer is sending us a topic update. This should not happen.
           Ignore.
           TODO greylist/simulation error?
        *)
        state
    | topics ->
        let topics =
          Topic_map.fold
            (fun topic present acc ->
              if present then Topic_set.add topic acc
              else Topic_set.remove topic acc)
            topic_diff
            topics
        in
        State.set_known_topics state sender topics
  in
  let rec maintenance_clock () =
    let* now in
    logger.log Debug "%a.%f: maintenace tick@." Thread_id.pp self now ;
    let* () = sleep config.maintenance_dt in
    let* () = send Messages.Internal_maintenance_clock self 0.0 in
    maintenance_clock ()
  in
  let* now in
  let state = State.make ~self ~config ~now ~logger () in
  join
    (maintenance_clock ())
    (let* _ = message_handler state in
     return ())
