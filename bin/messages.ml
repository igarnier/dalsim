open Des

module Topic_set = struct
  include Set.Make (Int)

  let pp fmtr set =
    let elts = elements set in
    Format.fprintf
      fmtr
      "@[{ %a }@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",@ ")
         Format.pp_print_int)
      elts
end

let pp_pair pp_x pp_y fmtr (x, y) = Format.fprintf fmtr "(%a, %a)" pp_x x pp_y y

module Topic_map = struct
  include Map.Make (Int)

  let pp pp_elt fmtr map =
    let elts = bindings map in
    Format.fprintf
      fmtr
      "@[{ %a }@]"
      (Format.pp_print_list
         ~pp_sep:(fun fmtr () -> Format.fprintf fmtr ",@ ")
         (pp_pair Format.pp_print_int pp_elt))
      elts
end

type t =
  | Connect  (** Request for establishing a connection *)
  | Nack of Topic_set.t Thread_id.Map.t  (** Negative answer after [Connect] *)
  | Ack  (** Positive answer after [Connect] *)
  | Disconnect  (** Disconnection notification *)
  | Bootstrap of Topic_set.t  (** Set of topics peer is interested in *)
  | Advertise of Topic_set.t Thread_id.Map.t  (** Set of peers with topics *)
  | Swap_request of Thread_id.t  (** Request to swap payload *)
  | Swap_ack of Thread_id.t  (** Accept to swap together with exchanged peer *)
  | UpdateTopics of bool Topic_map.t  (** Notification of topic update *)
  | Internal_maintenance_clock
[@@deriving show, ord] [@@ocaml.warning "-37"]
