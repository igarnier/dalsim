open Des

module Topic_set : sig
  include Set.S with type elt = int

  val pp : Format.formatter -> t -> unit
end

module Topic_map : sig
  include Map.S with type key = int

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

type t =
  | Connect  (** Request for establishing a connection *)
  | Nack of Topic_set.t Thread_id.Map.t  (** Negative answer after [Connect] *)
  | Ack  (** Positive answer after [Connect] *)
  | Disconnect  (** Disconnection notification *)
  | Bootstrap of Topic_set.t  (** Set of topics peer is interested in *)
  | Advertise of Topic_set.t Thread_id.Map.t  (** Set of peers with topics *)
  | Swap_request of Thread_id.t  (** Request to swap payload *)
  | Swap_ack of Thread_id.t  (** Accept to swap together with exchanged peer *)
  | UpdateTopics of bool Topic_map.t  (** Notification of topic update *)
  | Internal_maintenance_clock

val compare : t -> t -> int

val pp : Format.formatter -> t -> unit
