let rec repeat n x = if n <= 0 then [] else x :: repeat (pred n) x

let rev_split_n n l =
  let rec loop acc n = function
    | [] -> (acc, [])
    | rem when n <= 0 -> (acc, rem)
    | x :: xs -> loop (x :: acc) (pred n) xs
  in
  loop [] n l

let split_n n l =
  let (rev_taken, dropped) = rev_split_n n l in
  (List.rev rev_taken, dropped)

let rev_take_n n l = fst (rev_split_n n l)

let take_n n l = fst (split_n n l)

let rec drop_n n l =
  if n <= 0 then l else match l with [] -> [] | _ :: xs -> drop_n (n - 1) xs

let shuffle l rng =
  let a = Array.of_list l in
  let len = Array.length a in
  for i = len downto 2 do
    let m = Random.State.int rng i in
    let n' = i - 1 in
    if m <> n' then (
      let tmp = a.(m) in
      a.(m) <- a.(n') ;
      a.(n') <- tmp)
  done ;
  Array.to_list a

let shuffle_array a rng =
  let len = Array.length a in
  for i = len downto 2 do
    let m = Random.State.int rng i in
    let n' = i - 1 in
    if m <> n' then (
      let tmp = a.(m) in
      a.(m) <- a.(n') ;
      a.(n') <- tmp)
  done
