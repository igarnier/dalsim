open Messages
open Des
module Log = Des.Log

type known_state = { topics : Topic_set.t; last_seen : float option }

let empty_known = { topics = Topic_set.empty; last_seen = None }

type t =
  { self : Thread_id.t;
    config : Config.t;
    topics : Topic_set.t;
    last_swap_request : float;
    last_bootstrap_message : float;
    pending_swaps : Thread_id.t Thread_id.Map.t;
    pending_connections : Thread_id.Set.t;
    peers : Thread_id.Set.t; (* Invariant: included in support of [known] *)
    known : known_state Thread_id.Map.t;
    last_contacted : float Thread_id.Map.t;
    logger : Log.t
  }

let make ~self ~config ~now ?(topics = Topic_set.empty) ?(logger = Log.null) ()
    =
  let known =
    List.to_seq config.Config.bootstrap
    |> Seq.map (fun bootstrap -> (bootstrap, empty_known))
    |> Thread_id.Map.of_seq
  in
  { self;
    config;
    topics;
    last_swap_request = now;
    last_bootstrap_message = now;
    pending_swaps = Thread_id.Map.empty;
    pending_connections = Thread_id.Set.empty;
    peers = Thread_id.Set.empty;
    known;
    last_contacted = Thread_id.Map.empty;
    logger
  }

let get_known state p = Thread_id.Map.find p state.known

let set_known state p ks =
  assert (not (Thread_id.equal state.self p)) ;
  { state with known = Thread_id.Map.add p ks state.known }

let set_known_last_seen state p last_seen =
  let ks = get_known state p in
  set_known state p { ks with last_seen = Some last_seen }

let set_known_last_seen state p last_seen =
  let ks = get_known state p in
  set_known state p { ks with last_seen = Some last_seen }

let get_known_topics state p = (get_known state p).topics

let set_known_topics state p topics =
  let ks = get_known state p in
  set_known state p { ks with topics }

let add_known_topics state p topics =
  let ks = get_known state p in
  set_known state p { ks with topics = Topic_set.union topics ks.topics }
  [@@ocaml.warning "-32"]

let set_last_contacted state p t =
  { state with last_contacted = Thread_id.Map.add p t state.last_contacted }

let get_last_contacted state p = Thread_id.Map.find_opt p state.last_contacted

let last_seen state p = (get_known state p).last_seen

let is_peer_disconnected state p = not (Thread_id.Set.mem p state.peers)

let add_peer state p ?(topics = Topic_set.empty) time =
  assert (not (Thread_id.equal state.self p)) ;
  { state with
    peers = Thread_id.Set.add p state.peers;
    known = Thread_id.Map.add p { topics; last_seen = Some time } state.known
  }

let remove_peer state p time =
  let state = { state with peers = Thread_id.Set.remove p state.peers } in
  set_known_last_seen state p time

let add_pending_connection state p ~now =
  { state with
    pending_connections = Thread_id.Set.add p state.pending_connections;
    last_contacted = Thread_id.Map.add p now state.last_contacted
  }

let remove_pending_connection state p =
  { state with
    pending_connections = Thread_id.Set.remove p state.pending_connections
  }

let add_pending_swap state ~swapped ~with_ =
  { state with
    pending_swaps = Thread_id.Map.add with_ swapped state.pending_swaps
  }

let remove_pending_swap state ~with_ =
  { state with pending_swaps = Thread_id.Map.remove with_ state.pending_swaps }

let pending_swap state ~with_ = Thread_id.Map.find_opt with_ state.pending_swaps

let is_in_pending_swap state peer =
  Thread_id.Map.exists
    (fun with_ swapped -> Thread_id.equal swapped peer)
    state.pending_swaps

let disconnect state peer =
  let open Lang in
  if not (Thread_id.Map.mem peer state.known) then (
    Format.printf "error: %a not found in known@." Thread_id.pp peer ;
    exit 1) ;
  if not (Thread_id.Set.mem peer state.peers) then
    (* TODO: error? *)
    return state
  else
    let* delay = perform (Config.delay state.config) in
    let* () = send Messages.Disconnect peer delay in
    let state =
      { state with
        pending_swaps = Thread_id.Map.remove peer state.pending_swaps;
        peers = Thread_id.Set.remove peer state.peers
      }
    in
    let* now in
    let state = set_known_last_seen state peer now in
    return state

let broadcast_bootstrap_message state =
  let open Lang in
  let peers = Thread_id.Set.elements state.peers in
  let msg = Messages.Bootstrap state.topics in
  let rec loop peers =
    match peers with
    | [] -> return ()
    | peer :: tl ->
        let () =
          state.logger.log
            Debug
            "%a: sending bootstrap message to %a@."
            Thread_id.pp
            state.self
            Thread_id.pp
            peer
        in
        let* delay = perform (Config.delay state.config) in
        let* () = send msg peer delay in
        loop tl
  in
  loop peers
