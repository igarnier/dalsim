type t =
  { nodes : int;
    bootstrap : Des.Thread_id.t list;
    expected_transport_delay : float;
    target_conns : int;
    maintenance_dt : float;
    swap_linger : float;
    time_between_looking_for_peers : float;
    reconnection_cooldown : float
  }

let default =
  { nodes = 0;
    bootstrap = [];
    expected_transport_delay = 0.5;
    target_conns = 0;
    maintenance_dt = 1.0;
    swap_linger = 1.0;
    time_between_looking_for_peers = 1.0;
    reconnection_cooldown = 5.0
  }

let thread_encoding : Des.Thread_id.t Data_encoding.t =
  let open Data_encoding in
  conv (fun (t : Des.Thread_id.t) -> (t :> int)) Des.Thread_id.of_int int31

let encoding =
  let open Data_encoding in
  conv
    (fun { nodes;
           bootstrap;
           expected_transport_delay;
           target_conns;
           maintenance_dt;
           swap_linger;
           time_between_looking_for_peers;
           reconnection_cooldown
         } ->
      ( nodes,
        bootstrap,
        expected_transport_delay,
        target_conns,
        maintenance_dt,
        swap_linger,
        time_between_looking_for_peers,
        reconnection_cooldown ))
    (fun ( nodes,
           bootstrap,
           expected_transport_delay,
           target_conns,
           maintenance_dt,
           swap_linger,
           time_between_looking_for_peers,
           reconnection_cooldown ) ->
      { nodes;
        bootstrap;
        expected_transport_delay;
        target_conns;
        maintenance_dt;
        swap_linger;
        time_between_looking_for_peers;
        reconnection_cooldown
      })
    (obj8
       (req "nodes" int31)
       (req "bootstrap" (list thread_encoding))
       (req "expected_transport_delay" float)
       (req "target_conns" int31)
       (req "maintenance_dt" float)
       (req "swap_linger" float)
       (req "time_between_looking_for_peers" float)
       (req "reconnection_cooldown" float))

let pp fmtr (cfg : t) =
  Data_encoding.Json.pp fmtr (Data_encoding.Json.construct encoding cfg)

let load file =
  let contents = In_channel.with_open_text file In_channel.input_all in
  match Data_encoding.Json.from_string contents with
  | Ok json -> Data_encoding.Json.destruct encoding json
  | Error msg ->
      Format.eprintf "Config.load %s: %s" file msg ;
      exit 1

let delay config rng_state =
  (* TODO: It'd be smarter to store the rate instead of the expected delay *)
  Stats.Gen.exponential ~rate:(1. /. config.expected_transport_delay) rng_state
