open Des
module CLI = Minicli.CLI

let config =
  match Sys.argv with
  | [| _; "run"; config |] -> Config.load config
  | _ ->
      let open Format in
      eprintf "usage: <exe> run <config.json>\n" ;
      eprintf "config example:\n@[<v2>%a@]@," Config.pp Config.default ;
      exit 1

let peers = Array.init config.nodes (fun _ -> Thread_id.gen ())

let computation =
  Lang.run
    (Array.mapi
       (fun i self ->
         let logger =
           Log.file
             ~debug:(Printf.sprintf "debug_%.2d.txt" i)
             (Printf.sprintf "log_%.2d.txt" i)
         in
         if i = 0 then
           (Protocol.node ~logger { config with bootstrap = [] } self, self)
         else (Protocol.node ~logger config self, self))
       peers)
    (fun state ~min_sleep ~cont -> cont (Lang.Internal.elapse state min_sleep))

let _ = computation (Random.State.make [| 98129121; 0x1337; 0x533D |])
